void main() {
  var ambani = AppDetail();
  ambani.nameOfApp = "Ambani Africa App";
  ambani.category = "Educational App";
  ambani.developer = "Ambani Africa";
  ambani.year = "2021";

  print("MTN BUSINESS APP AWARDEE DETAIL");
  ambani.detailOnAwardee();
  
  print("\nCapital Letter Of App Name");
  ambani.upperCaseOfAppName();


}

class AppDetail {
  String? nameOfApp;
  String? category;
  String? developer;
  String? year;

  void detailOnAwardee() {
    print("Name of App: $nameOfApp");
    print("Category: $category");
    print("Developer: $developer");
    print("Year: $year");
  }

  void upperCaseOfAppName() {
    print("${nameOfApp?.toUpperCase()}");
  }
}
